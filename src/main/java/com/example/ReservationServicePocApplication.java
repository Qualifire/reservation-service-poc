package com.example;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.stream.Stream;

@SpringBootApplication
public class ReservationServicePocApplication {

	@Bean
	CommandLineRunner runner(ReservationRepository reservationRepo) {
		return args -> Stream.of("Lou","Batiste","Manec","Stephanie","Sarah","Brecht","Johan","Dirk","Martine")
							.forEach(name -> reservationRepo.save(new Reservation(name)));
	}

	public static void main(String[] args) {
		SpringApplication.run(ReservationServicePocApplication.class, args);
	}
}
