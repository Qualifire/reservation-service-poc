package com.example;

import com.vaadin.annotations.Theme;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Grid;
import com.vaadin.ui.UI;
import org.springframework.beans.factory.annotation.Autowired;

@SpringUI(path = "ui")
@Theme("valo")
public class ReservationUI extends UI{

    private ReservationRepository reservationRepository;

    @Autowired
    public ReservationUI(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        Grid table = new Grid();
        BeanItemContainer<Reservation> container = new BeanItemContainer<Reservation>(
                                                            Reservation.class,
                                                            reservationRepository.findAll()
                                                    );

        table.setContainerDataSource(container);
        table.setSizeFull();
        setContent(table);
    }
}
